# Uang Service

[![pipeline status](https://gitlab.com/advprog_12-unus/uangunus/badges/master/pipeline.svg)](https://gitlab.com/advprog_12-unus/uangunus/-/commits/master)
[![coverage report](https://gitlab.com/advprog_12-unus/uangunus/badges/master/coverage.svg)](https://gitlab.com/advprog_12-unus/uangunus/-/commits/master)

Hanya microservice dan harap dijalankan bersama registry
sebelum membuka main service

Deployed on : https://unus-uang.herokuapp.com/