package com.unus.uangservice;

import com.unus.uangservice.repository.UangRepository;
import com.unus.uangservice.service.UangServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class UangserviceApplicationTests {

	@MockBean
	private UangRepository uangRepository;

	@MockBean
	private UangServiceImpl uangService;

	@Test
	void contextLoads() {
	}

	@Test
	public void mainFunctionTest() {
		UangserviceApplication.main(new String[] {});
	}


}
