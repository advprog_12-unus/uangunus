package com.unus.uangservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unus.uangservice.core.UangBase;
import com.unus.uangservice.repository.UangRepository;
import com.unus.uangservice.service.UangServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = UangController.class)
public class UangControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UangRepository uangRepository;

    @MockBean
    private UangServiceImpl uangService;

    @Test
    void allUangTest() throws Exception {
        mockMvc.perform(get("/service-uangs/allUangs/1/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(new ResultHandler() {
                    @Override
                    public void handle(MvcResult result) throws Exception {
                        assertEquals("[]",result.getResponse().getContentAsString());
                    }
                });
    }


    @Test
    void createUangTest() throws Exception {
        UangBase uangBase = new UangBase();
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(uangBase);

        mockMvc.perform(post("/service-uangs/create-uang/1/1").content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void addUangTest() throws Exception {
        UangBase uangBase = new UangBase();
        uangService.register(uangBase);
        lenient().when(uangService.register(uangBase)).thenReturn(uangBase);
        mockMvc.perform(get("/service-uangs/add-uang/1/1"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void deleteUangTest() throws Exception {
        mockMvc.perform(delete("/service-uangs/delete-uang/1"))
                .andExpect(status().is4xxClientError());
    }

}
