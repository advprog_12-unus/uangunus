package com.unus.uangservice.service;

import com.unus.uangservice.core.UangBase;
import com.unus.uangservice.repository.UangRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class UangServiceImplTest {

    @Mock
    private UangRepository uangRepository;

    private UangBase uangBase;

    @InjectMocks
    private UangServiceImpl uangService;

    @BeforeEach
    void setUp() {
        uangBase = new UangBase();
        uangBase.setNameOfFintech("OVO");
    }

    @Test
    void registerTest() {
        uangService.register(uangBase);
        lenient().when(uangService.register(uangBase)).thenReturn(uangBase);
    }

    @Test
    void getAllUangsOfUserTest() {
        List<UangBase> uangList = uangService.getAllUangOfUser((long)1, (long)1);
        lenient().when(uangService.getAllUangOfUser((long)1, (long)1)).thenReturn(uangList);
    }

    @Test
    void deleteUangTest() {
        uangService.register(uangBase);
        uangService.deleteUang(uangBase.getId());
        lenient().when(uangService.register(uangBase)).thenReturn(uangBase);
    }

}
