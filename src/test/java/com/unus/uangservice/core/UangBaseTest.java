package com.unus.uangservice.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UangBaseTest {

    private UangBase uangBase;

    @BeforeEach
    void setUp() {
        uangBase = new UangBase();
        uangBase.setId(0);
        uangBase.setNameOfFintech("OVO");
        uangBase.setAmountOfMoney(10000);
        uangBase.setUser((long) 1);
        uangBase.setDompet((long) 2);
    }

    @Test
    void setIdTest(){

        uangBase.setId(1);
        assertEquals(1, uangBase.getId());
        uangBase.setId(0);
    }

    @Test
    void getIdTest(){

        assertEquals(0, uangBase.getId());
    }

    @Test
    void setAmoutOfMoneyTest(){

        uangBase.setAmountOfMoney(20000);
        assertEquals(20000, uangBase.getAmountOfMoney());
        uangBase.setAmountOfMoney(10000);
    }

    @Test
    void getAmoutOfMoneyTest(){

        assertEquals(10000, uangBase.getAmountOfMoney());
    }

    @Test
    void setNameOfFintechTest(){

        uangBase.setNameOfFintech("GOPAY");
        assertEquals("GOPAY", uangBase.getNameOfFintech());
        uangBase.setNameOfFintech("OVO");
    }

    @Test
    void getNameOfFintechTest(){

        assertEquals("OVO", uangBase.getNameOfFintech());
    }

    @Test
    void setUserTest(){

        uangBase.setUser((long) 2);
        assertEquals(2, uangBase.getUser());
        uangBase.setUser((long)1);
    }

    @Test
    void getUserTest(){

        assertEquals(1, uangBase.getUser());
    }

    @Test
    void setDompetTest(){

        uangBase.setDompet((long) 3);
        assertEquals(3, uangBase.getDompet());
        uangBase.setDompet((long) 2);
    }

    @Test
    void getDompetTest(){

        assertEquals(2, uangBase.getDompet());
    }

}
