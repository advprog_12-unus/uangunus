package com.unus.uangservice.controller;


import com.unus.uangservice.core.UangBase;
import com.unus.uangservice.repository.UangRepository;
import com.unus.uangservice.service.UangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/service-uangs")
public class UangController {

    @Autowired
    private UangService uangService;

    @Autowired
    private UangRepository uangRepository;

    public UangController(UangService uangService){
        this.uangService = uangService;
    }

    @GetMapping("/allUangs/{idUser}/{idDompet}")
    private List<UangBase> allUang(@PathVariable("idUser") Long idUser,  @PathVariable("idDompet") Long idDompet) {
        return uangService.getAllUangOfUser(idUser, idDompet);
    }

    // Route ke form uang
    @GetMapping("/create-uang/{idUser}/{idDompet}")
    public UangBase createUang(@PathVariable("idUser") Long idUser, @PathVariable("idDompet") Long idDompet, Model model) {
        UangBase uangBas = new UangBase();
        System.out.println("user : " + idUser);
        System.out.println("dompet : " + idDompet);
        uangBas.setUser(idUser);
        uangBas.setDompet(idDompet);
        return uangBas;
    }

    /** Route untuk menyimpan uang yang ditambahkan dan kembali ke halaman list uangs. */
    @PostMapping("/add-uang/{idUser}/{idDompet}")
    public void addUang(@PathVariable("idUser") Long idUser, @PathVariable("idDompet") Long idDompet, @RequestBody UangBase uang) {
        uang.setUser(idUser);
        uang.setDompet(idDompet);
        uangService.register(uang);
    }

    // Route untuk membuang uang yang dipilih
    @GetMapping("/delete-uang/{id}")
    public void deleteUang(@PathVariable Long id) {
        uangService.deleteUang(id);
    }

}
