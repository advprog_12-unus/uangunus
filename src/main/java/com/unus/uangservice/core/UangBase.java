package com.unus.uangservice.core;

import javax.persistence.*;


@Entity
@Table(name = "uangs")
public class UangBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String nameOfFintech;

    @Column(nullable = false)
    private long amountOfMoney;

    @Column(nullable = false)
    private long userid;

    @Column(nullable = false)
    private long dompetid;

    public void setId(long idMoney){
        this.id = idMoney;
    }

    public long getId(){
        return this.id;
    }

    public void setNameOfFintech(String name){
        this.nameOfFintech = name;
    }

    public String getNameOfFintech() {
        return this.nameOfFintech;
    }

    public void setAmountOfMoney(long amount) {
        this.amountOfMoney = amount;
    }

    public long getAmountOfMoney() {
        return this.amountOfMoney;
    }

    public void setUser(long idUser){
        this.userid = idUser;
    }

    public long getUser(){
        return this.userid;
    }

    public void setDompet(long idUser){
        this.dompetid = idUser;
    }

    public long getDompet(){
        return this.dompetid;
    }


}
