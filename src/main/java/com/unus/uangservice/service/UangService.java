package com.unus.uangservice.service;

import com.unus.uangservice.core.UangBase;

import java.util.List;

public interface UangService {

    public void deleteUang(Long id); //delete

    public UangBase register(UangBase uang); //create

    public List<UangBase> getAllUangOfUser(Long idUser, Long idDompet);

}
