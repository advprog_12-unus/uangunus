package com.unus.uangservice.service;

import com.unus.uangservice.core.UangBase;
import com.unus.uangservice.repository.UangRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UangServiceImpl implements UangService {

    @Autowired
    private UangRepository uangRepository;

    public UangServiceImpl(UangRepository uangRepository){
        this.uangRepository = uangRepository;
    }

    @Override
    public void deleteUang(Long id) {
        uangRepository.deleteById(id);
    }

    @Override
    public UangBase register(UangBase uang) {
        return uangRepository.save(uang);
    }

    @Override
    public List<UangBase> getAllUangOfUser(Long idUser, Long idDompet) {
        return uangRepository.findAllByUseridAndDompetid(idUser, idDompet);
    }
}
