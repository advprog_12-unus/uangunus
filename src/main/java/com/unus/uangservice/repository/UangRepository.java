package com.unus.uangservice.repository;

import com.unus.uangservice.core.UangBase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UangRepository extends JpaRepository<UangBase, Long> {
    List<UangBase> findAllByUseridAndDompetid(Long userid, Long dompetid);
}

